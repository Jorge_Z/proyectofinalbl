/*
 * 
 */
package com.ProyectoCOI.ui;

import java.io.*;

/**
 *  Clase encargada de los métodos de entrada y salida de datos
 * @author Jorge Zúñiga
 * @version 1
 */
public class UI {

    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private PrintStream out = System.out;

    public void mostrarMenu() {
        out.println("----¡Bienvenido al APP del COI!----");
        out.println("Por favor, seleccione una opción");
        out.println("1. Registrar un usuario");
        out.println("2. Listar los usuarios");
        out.println("3. Registrar administrador");
        out.println("4. Mostrar el administrador");
        out.println("5. Registrar la actividad deportiva");
        out.println("6. Listar las actividades deportivas");
        out.println("7. Registrar división geográfica");
        out.println("8. Listar las divisiones geográficas");
        out.println("9. Registrar el grupo");
        out.println("10. Listar grupos");
        out.println("11. Registrar el reto");
        out.println("12. Listar los retos");
        out.println("13. Registrar el hito");
        out.println("14. Listar los hitos");
        out.println("0. Salir");
    }

    public int leerOpcion() throws IOException {
        return Integer.parseInt(in.readLine());
    }

    public void impMens(String mens) {
        out.println(mens);
    }

    public String leerTexto() throws IOException {
        return in.readLine();
    }
}
