package com.ProyectoCOI.bl.entities;

/** @author Ahías Steller Mora
 * @version v1.5
 * @since 13/02/2021
 *
 * Esta clase gestiona las características de la clase de Actividad Deportiva
 *
 **/

public class ActividadDeportiva {

    private int codigo;
    private String nombre;
    private String icono;

    /**
     * Este es el método contructor de la clase
     * Es el constructor por defecto, no recibe parámetros
     **/

    public ActividadDeportiva() {
        this.codigo = 0;
        this.nombre = "";
        this.icono = "";
    }

    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros
     * @param codigo es de tipo entero y representa el código de la actividad deportiva
     * @param nombre es el tipo String y representa el nombre de la actividad deportiva
     * @param icono es de tipo String y representa el ícono de la actividad deportiva
     **/

    public ActividadDeportiva(int codigo, String nombre, String icono) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.icono = icono;
    }

    /**
     * @return El código almacenado en el sistema para el administrador.
     */

    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo Guarda el código de la actividad deportiva.
     */

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return El nombre almacenado en el sistema para la actividad deportiva.
     */

    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre Guarda el nombre de la actividad deportiva.
     */

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return El ícono almacenado en el sistema para la actividad deportiva.
     */

    public String getIcono() {
        return icono;
    }

    /**
     * @param icono Guarda el ícono de la actividad deportiva.
     */

    public void setIcono(String icono) {
        this.icono = icono;
    }

    /**
     * Método para devolver en un String los atributos del objeto
     * @return devuelve un String con la información del objeto
     **/

    @Override
    public String toString() {
        return "codigo=" + codigo +
                ", nombre=" + nombre +
                ", icono=" + icono;
    }

    /**
     * Método equals para comparar actividades deportivas registradas en el sistema
     * @param obj instancia de la clase Objeto, para almacenar temporalmente el objeto a comparar
     * @return true si la actividad deportiva tiene el mismo código, false si no o no es una actividad deportiva
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActividadDeportiva other = (ActividadDeportiva) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }
    
    
}
