/*
 * 
 */
package com.ProyectoCOI.bl.entities;

import java.util.Objects;

/**
 *
 * @author Jorge Zúñiga
 * @version 1
 * 
 * Esta clase abstracta es la superclase para los usuarios del sistema.
 */
public abstract class Usuario {
    protected String id;
    protected String correo;
    protected String contrasenna;
    protected String nombre;
    protected String apellidos;
    protected String pais;

    public Usuario() {
    }
    
    public Usuario(String id, String correo, String contrasenna, String nombre, String apellidos, String pais) {
        this.id = id;
        this.correo = correo;
        this.contrasenna = contrasenna;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.pais = pais;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        return "id=" + id + ", correo=" + correo + ", contrasenna=" + contrasenna + 
                ", nombre=" + nombre + ", apellidos=" + apellidos + ", pais=" + pais;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        return true;
    }
    
       
    
}
