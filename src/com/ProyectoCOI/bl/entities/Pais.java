/*
 * 
 */
package com.ProyectoCOI.bl.entities;

import java.util.Objects;

/**
 *
 * @author Jorge Zúñiga
 * @version 1.5
 * 
 * Esta clase gestiona los países desde los que se registren los usuarios
 */
public class Pais {
    private int codigo;
    private String nombre;
    
    /**
     * Constructor por defecto, sin parámetros 
     */
    public Pais() {
    }
    
    /**
     * Constructor con todos los parámetros
     * @param codigo El código, generado por el sistema, para cada país
     * @param nombre El nombre del país desde el que registra el cliente
     */
    public Pais(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }
    
    /**
     * 
     * @return El código creado por el sistema para cada país
     */
    public int getCodigo() {
        return codigo;
    }
    
    /**
     * 
     * @param codigo Guarda el código creado por el sistema para cada país
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    /**
     * 
     * @return El nombre del país desde el que se registró el cliente
     */
    public String getNombre() {
        return nombre;
    }
    
    /**
     * 
     * @param nombre Guarda el nombre del país desde el que se registró el cliente
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * 
     * @return El valor de cada atributo de un objeto "Usuario", convertido a un String
     */
    @Override
    public String toString() {
        return "codigo=" + codigo + ", nombre=" + nombre;
    }
    
    /**
     * Método equals para comparar países registrados en el sistema
     * @param obj instancia de la clase Objeto, para almacenar temporalmente el objeto a comparar
     * @return true si dos países tienen el mismo nombre, false si no
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pais other = (Pais) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
    
}
