/*
 * 
 */
package com.ProyectoCOI.bl.entities;

/**
 *
 * @author Jorge Zúñiga
 * @version 1.5
 * 
 * Esta clase permite gestionar las divisiones geográficas para los clientes que registren desde Costa Rica
 */
public class DivisionGeo {
    private int codigo;
    private String nombre;
    private String tipo;
    
    /**
     * Constructor por defecto, sin parámetros
     */
    public DivisionGeo() {
    }
    
    /**
     * Constructor con todos los parámetros
     * @param codigo El código asignado a cada división geográfica
     * @param nombre El nombre de cada división geográfica
     * @param tipo El tipo de cada división geográfica a registrar: provincia, cantón o distrito
     */
    public DivisionGeo(int codigo, String nombre, String tipo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.tipo = tipo;
    }
     /**
    *@return El código asignado a cada división geográfica
    */
    public int getCodigo() {
        return codigo;
    }
    
    /**
     * 
     * @param codigo Guarda el código de cada división geográfica
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    /**
     * 
     * @return El nombre de la división geográfica
     *
     * */
    public String getNombre() {
        return nombre;
    }
    
    /**
     * 
     * @param nombre Gaurda el nombre asignado a cada división geográfica
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * 
     * @return El tipo de división geográfica registrada
     */
    public String getTipo() {
        return tipo;
    }
    
    /**
     * 
     * @param tipo Guarda el tipo de división geográfica a registrar
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
       
    /**
     * 
     * @return El valor de los atributos de un objeto "DivisionGeo", convertido a un String
     */
    @Override
    public String toString() {
        return "codigo=" + codigo + ", nombre=" + nombre + ", tipo=" + tipo;
    }
    
    /**
     * Método equals para comparar las divisiones geográficas guardadas en el sistema.
     * @param obj instancia de la clase Objeto, para almacenar temporalmente el objeto a comparar
     * @return true si la división geográfica tiene el mismo código, false si no
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DivisionGeo other = (DivisionGeo) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }
    
    
}
