package com.ProyectoCOI.bl.entities;

import java.util.ArrayList;

/** @author Ahías Steller Mora
 * @version v1.0
 * @since 13/02/2021
 *
 * Esta clase gestiona las características de la clase Reto
 *
 **/

public class Reto {

    private int codigo;
    private String nombre;
    private String descripcion;
    private String foto;
    private double distancia;
    private String medalla;
    private double costo;
    private String inicio;
    private String fin;
    private ArrayList<Atleta> listaUsuarios;
    private ArrayList<Grupo> listaGrupos;
    private ArrayList<Hito> listaHitos;

    /**
     * Este es el método contructor de la clase
     * Es el constructor por defecto, no recibe parámetros
     **/

    public Reto() {
        this.codigo = 0;
        this.nombre = "";
        this.descripcion = "";
        this.foto = "";
        this.distancia = 0.0;
        this.medalla = "";
        this.costo = 0.0;
        this.inicio = "";
        this.fin = "";
        this.listaUsuarios = new ArrayList<>();
        this.listaGrupos = new ArrayList<>();
        this.listaHitos = new ArrayList<>();
    }
    
    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros, excepto las colecciones
     * @param codigo es de tipo Entero y representa el código del reto
     * @param nombre es de tipo String y representa el nombre del grupo
     * @param descripcion es de tipo String y representa la descripción del reto
     * @param foto es de tipo String y representa la foto del reto
     * @param distancia de tipo decimal y representa la distancia recorrida en el reto
     * @param medalla es de tipo String y representa la medalla obtenida en el reto
     * @param costo es de tipo decimal y representa el costo de realizar el reto
     * @param inicio es de tipo String y representa el punto de inicio
     * @param fin es de tipo String y representa el punto final del reto
     **/

    public Reto(int codigo, String nombre, String descripcion, String foto, double distancia, String medalla, double costo, String inicio, String fin) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.foto = foto;
        this.distancia = distancia;
        this.medalla = medalla;
        this.costo = costo;
        this.inicio = inicio;
        this.fin = fin;
        this.listaUsuarios = new ArrayList<>();
        this.listaGrupos = new ArrayList<>();
        this.listaHitos = new ArrayList<>();
    }

    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros, incluyendo las colecciones como parámetros
     * @param codigo es de tipo Entero y representa el código del reto
     * @param nombre es de tipo String y representa el nombre del grupo
     * @param descripcion es de tipo String y representa la descripción del reto
     * @param foto es de tipo String y representa la foto del reto
     * @param distancia de tipo decimal y representa la distancia recorrida en el reto
     * @param medalla es de tipo String y representa la medalla obtenida en el reto
     * @param costo es de tipo decimal y representa el costo de realizar el reto
     * @param inicio es de tipo String y representa el punto de inicio
     * @param fin es de tipo String y representa el punto final del reto
     * @param listaUsuarios Una colección para almacenar los usuarias(os) matriculados en el reto
     * @param listaGrupos Una colección para almacenar los grupos matriculados en el reto
     * @param listaHitos Una colección para almacenar los hitos alcanzados en la realización del reto
     **/

    public Reto(int codigo, String nombre, String descripcion, String foto, double distancia, String medalla, double costo, String inicio, String fin, 
            ArrayList listaUsuarios, ArrayList listaGrupos, ArrayList listaHitos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.foto = foto;
        this.distancia = distancia;
        this.medalla = medalla;
        this.costo = costo;
        this.inicio = inicio;
        this.fin = fin;
        this.listaUsuarios = new ArrayList<>();
        this.listaGrupos = new ArrayList<>();
        this.listaHitos = new ArrayList<>();
    }

    /**
     * @return El código almacenado en el sistema para el identificar al reto.
     */

    public int getCodigo() {
        return 0;
    }

    /**
     * @param codigo Guarda el código del reto.
     */

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return El nombre en el sistema para el llamar al reto.
     */

    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre Guarda el nombre del reto.
     */

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return La descripción almacenada en el sistema del reto
     */

    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion Guarda la descripción del reto.
     */

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return La foto almacenada en el sistema del reto
     */

    public String getFoto() {
        return foto;
    }

    /**
     * @param foto Guarda la foto del reto.
     */

    public void setFoto(String foto) {
        this.foto = foto;
    }

    /**
     * @return La distancia en el sistema del reto
     */

    public double getDistancia() {
        return distancia;
    }

    /**
     * @param distancia Guarda la distancia del reto.
     */

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    /**
     * @return La medalla alcanzada en el sistema del reto
     */

    public String getMedalla() {
        return medalla;
    }

    /**
     * @param medalla Guarda la medalla del reto.
     */

    public void setMedalla(String medalla) {
        this.medalla = medalla;
    }

    /**
     * @return El costo en el sistema del reto
     */

    public double getCosto() {
        return costo;
    }

    /**
     * @param costo Guarda el costo del reto.
     */

    public void setCosto(double costo) {
        this.costo = costo;
    }

    /**
     * @return El punto inicio en el mapa del sistema del reto
     */

    public String getInicio() {
        return inicio;
    }

    /**
     * @param inicio Guarda el punto de inicio del reto.
     */

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    /**
     * @return El punto final en el mapa del sistema del reto
     */

    public String getFin() {
        return fin;
    }

    /**
     * @param fin Guarda el punto final del reto.
     */

    public void setFin(String fin) {
        this.fin = fin;
    }

    /**
     * @return los(as) usuarios(as) matriculados a un reto.
     */

    public ArrayList<Atleta> getListaUsuarios() {
        return listaUsuarios;
    }

    /**
     * @param listaUsuarios Guarda los usuarios matriculados a un reto.
     */

    public void setListaUsuarios(ArrayList<Atleta> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    /**
     * @return los grupos matriculados a un reto.
     */

    public ArrayList<Grupo> getListaGrupos() {
        return listaGrupos;
    }

    /**
     * @param listaGrupos Guarda los grupos matriculados a un reto.
     */

    public void setListaGrupos(ArrayList<Grupo> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }

    /**
     * @return los hitos alcanzados en un reto.
     */

    public ArrayList<Hito> getListaHitos() {
        return listaHitos;
    }

    /**
     * @param listaHitos Guarda los hitos de un reto.
     */

    public void setListaHitos(ArrayList<Hito> listaHitos) {
        this.listaHitos = listaHitos;
    }
      
    @Override
    public String toString() {
        return "codigo=" + codigo +
                ", nombre=" + nombre +
                ", descripcion=" + descripcion +
                ", foto=" + foto +
                ", distancia=" + distancia +
                ", medalla=" + medalla +
                ", costo=" + costo +
                ", inicio=" + inicio +
                ", fin=" + fin +
                ", lista de Usuarios="+ listaUsuarios+
                ", lista de Grupos="+ listaGrupos+
                "lista de Hitos="+ listaHitos;
    }
    
    /**
     * Método equals para comparar Retos registrados en el sistema
     * @param obj instancia de la clase Objeto, para almacenar temporalmente el objeto a comparar
     * @return true si los dos Retos tienen el mismo código, false si no
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reto other = (Reto) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }
    
    
}


