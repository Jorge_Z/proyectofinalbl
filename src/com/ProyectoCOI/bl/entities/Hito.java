package com.ProyectoCOI.bl.entities;

import java.util.Objects;

/** @author Ahías Steller Mora
 * @version v1.0
 * @since 13/02/2021
 *
 * Esta clase gestiona las características de la clase Hito
 *
 **/


public class Hito {

    private String ubicacion;
    private String imagen;
    private String descripcion;
    private String link;

    /**
     * Este es el método contructor de la clase
     * Es el constructor por defecto, no recibe parámetros
     **/

    public Hito() {
        this.ubicacion = "";
        this.imagen = "";
        this.descripcion = "";
        this.link = "";
    }

    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros
     * @param ubicacion es de tipo String y representa la ubicación del hito alcanzado
     * @param imagen es el tipo String y representa la imágen del grupo
     * @param descripcion es de tipo String y representa la descripción del hito alcanzado
     * @param link es de tipo String y representa un sitio web del hito del lugar alcanzado
     **/

    public Hito(String ubicacion, String imagen, String descripcion, String link) {
        this.ubicacion = ubicacion;
        this.imagen = imagen;
        this.descripcion = descripcion;
        this.link = link;
    }

    /**
     * @return La ubicación almacenada en el sistema para del hito alcanzado.
     */

    public String getUbicacion() {
        return ubicacion;
    }

    /**
     * @param ubicacion Guarda la ubicación del hito.
     */

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    /**
     * @return La imagen almacenada en el sistema para del hito alcanzado.
     */

    public String getImagen() {
        return imagen;
    }

    /**
     * @param imagen Guarda la imagen del hito.
     */

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     * @return La descripción almacenada en el sistema para del hito alcanzado.
     */

    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion Guarda la descripción del hito.
     */

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return El link almacenado en el sistema para del hito alcanzado.
     */

    public String getLink() {
        return link;
    }

    /**
     * @param link Guarda el link sobre el sitio del hito.
     */

    public void setLink(String link) {
        this.link = link;
    }

    /**
     * Método para devolver en un String los atributos del objeto
     * @return devuelve un String con la información del objeto
     **/

    @Override
    public String toString() {
        return "ubicacion=" + ubicacion +
                ", imagen=" + imagen +
                ", descripcion=" + descripcion +
                ", link=" + link;
    }

    /**
     * Método para comparar hitos registrados en el sistema
     * @param obj instancia de la clase Objeto, para almacenar temporalmente el objeto a comparar
     * @return true si dos hitos tienen la misma ubicación, false si no
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Hito other = (Hito) obj;
        if (!Objects.equals(this.ubicacion, other.ubicacion)) {
            return false;
        }
        return true;
    }
    
    
}
