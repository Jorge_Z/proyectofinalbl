package com.ProyectoCOI.bl.entities;

import java.util.ArrayList;
import java.util.Objects;

/** @author Ahías Steller Mora
 * @version v1.5
 * @since 13/02/2021
 *
 * Esta clase gestiona las características de la clase Grupo
 *
 **/

public class Grupo {

    private String nombre;
    private String imagen;
    private ArrayList<Atleta>listaUsuarios;

    /**
     * Este es el método contructor de la clase
     * Es el constructor por defecto, no recibe parámetros
     **/

    public Grupo() {
        this.nombre = "";
        this.imagen = "";
        this.listaUsuarios = new ArrayList<>();
    }

    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros, excepto la colección de Atletas
     * @param nombre es de tipo String y representa el nombre del grupo
     * @param imagen es el tipo String y representa la imágen del grupo
     **/

    public Grupo(String nombre, String imagen) {
        this.nombre = nombre;
        this.imagen = imagen;
    }

    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros, incluyendo la colección de Atletas
     * @param nombre es de tipo String y representa el nombre del grupo
     * @param imagen es el tipo String y representa la imágen del grupo
     * @param listaUsuarios Una colección para almacenar los usuarias(os) registrados en un grupo
     **/

    public Grupo(String nombre, String imagen, ArrayList listaUsuarios) {
        this.nombre = nombre;
        this.imagen = imagen;
        this.listaUsuarios = listaUsuarios;
    }

    /**
     * @return El nombre almacenado en el sistema para el identificar al grupo.
     */

    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre Guarda el nombre del grupo.
     */

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return La imagen almacenada en el sistema para el identificar al grupo.
     */

    public String getImagen() {
        return imagen;
    }

    /**
     * @param imagen Guarda la imagen del grupo.
     */

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     * @return los(as) usuarios(as) vinculados a un grupo.
     */

    public ArrayList<Atleta> getListaUsuarios() {
        return listaUsuarios;
    }

    /**
     *
     * @param listaUsuarios Guarda los usuarios registados en un grupo.
     */

    public void setListaUsuarios(ArrayList<Atleta> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    /**
     * Método para devolver en un String los atributos del objeto
     * @return devuelve un String con la información del objeto
     **/


    @Override
    public String toString() {
        return "nombre=" + nombre + ", imagen=" + imagen + ", lista de Usuarios="+ listaUsuarios;
    }
    
    /**
     * Método para comparar grupos registrados en el sistema
     * @param obj instancia de la clase Objeto, para almacenar temporalmente el objeto a comparar
     * @return true si dos grupos tienen el mismo nombre, false si no
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Grupo other = (Grupo) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
    
}
