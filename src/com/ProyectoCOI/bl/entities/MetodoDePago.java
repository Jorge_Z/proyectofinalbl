package com.ProyectoCOI.bl.entities;

import java.time.LocalDate;

/** @author Ahías Steller Mora
 * @version v1.5
 * @since 13/02/2021
 *
 * Esta clase gestiona las características de la clase Método de Pago
 *
 **/

public class MetodoDePago {

    private String nombre;
    private int numero;
    private int cvv;
    private String tipo;
    private LocalDate fechaExpiracion;

    /**
     * Este es el método contructor de la clase
     * Es el constructor por defecto, no recibe parámetros
     **/

    public MetodoDePago() {
        this.nombre = "";
        this.numero = 0;
        this.cvv = 0;
        this.tipo = "";
        this.fechaExpiracion = LocalDate.now();
    }

    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros
     * @param nombre es de tipo String y representa el nombre del dueño de la tarjeta
     * @param numero es el tipo Entero y representa el número de la tarjeta
     * @param cvv es de tipo Entero y representa el código de seguridad de la tarjeta
     * @param tipo es de tipo String y representa la compañía de la tarjeta
     * @param fechaExpiracion es de tipo LocalDate y representa la fecha de expiración de la tarjeta
     **/

    public MetodoDePago(String nombre, int numero, int cvv, String tipo, LocalDate fechaExpiracion) {
        this.nombre = nombre;
        this.numero = numero;
        this.cvv = cvv;
        this.tipo = tipo;
        this.fechaExpiracion = fechaExpiracion;
    }

    /**
     * @return El nombre almacenada en el sistema titular de la tarjeta.
     */

    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre Guarda nombre de la tarjeta.
     */

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return El número almacenada en el sistema de la tarjeta.
     */

    public int getNumero() {
        return numero;
    }

    /**
     * @param numero Guarda el número de la tarjeta.
     */

    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * @return El CVV almacenado en el sistema de la tarjeta.
     */

    public int getCvv() {
        return cvv;
    }

    /**
     * @param cvv Guarda el CVV de la tarjeta.
     */

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    /**
     * @return El tipo (compañía) almacenado en el sistema de la tarjeta.
     */

    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo Guarda el tipo de la tarjeta.
     */

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * 
     * @return la fecha de expiración almacenada en el sistema
     */
    public LocalDate getFechaExpiracion() {
        return fechaExpiracion;
    }
    
    /**
     * 
     * @param fechaExpiracion guarda la fecha de expiración de la tarjeta
     */
    public void setFechaExpiracion(LocalDate fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    
    /**
     * Método para devolver en un String los atributos del objeto
     * @return devuelve un String con la información del objeto
     **/

    @Override
    public String toString() {
        return "nombre=" + nombre +
                ", numero=" + numero +
                ", cvv=" + cvv +
                ", tipo=" + tipo +
                ", fecha de expiración= "+ fechaExpiracion;
    }
    
    /**
     * Método equals para comparar tarjetas en el sistema
     * @param obj instancia de la clase Objeto, para almacenar temporalmente el objeto a comparar
     * @return true si el objeto recibido tiene el mismo número de tarjeta, false si no o no es una tarjeta
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MetodoDePago other = (MetodoDePago) obj;
        if (this.numero != other.numero) {
            return false;
        }
        return true;
    }
    
    
}
