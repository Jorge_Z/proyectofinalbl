package com.ProyectoCOI.bl.entities;

/** @author Ahías Steller Mora
 * @version v1.5
 * @since 13/02/2021
 *
 * Esta clase gestiona las características de la clase de Administrador. Es una subclase de la clase abstracta Usuario.
 *
 **/

public class Administrador extends Usuario{
    private boolean registrado;
    //constructores

    /**
     * Este es el método contructor de la clase
     * Es el constructor por defecto, no recibe parámetros
     **/

    public Administrador() {
        this.correo = "";
        this.contrasenna = "";
        this.nombre = "";
        this.apellidos = "";
        this.id = "";
        this.pais = "";
        this.registrado = false;
    }

    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros
     * @param correo es de tipo string y representa el correo del administrador
     * @param contrasenna es el tipo String y representa la contraseña del administrador
     * @param nombre es de tipo String y representa el nombre del administrador
     * @param apellidos es de tipo String y representa los apellidos del administrador
     * @param id es de tipo String y representa la identificación del administrador 
     * @param pais es de tipo String y representa el país del administrador
     **/

    public Administrador(String correo, String contrasenna, String nombre, String apellidos, String id, String pais) {
        this.correo = correo;
        this.contrasenna = contrasenna;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.id = id;
        this.pais = pais;
        this.registrado = true;
    }

    //métodos de acceso y get y set

    /**
     * @return El correo almacenado en el sistema del administrador.
     */
    @Override
    public String getCorreo() {
        return correo;
    }

    /**
     *
     * @param correo Guarda el correo ingresado del administrador.
     */
    @Override
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     *
     * @return La contraseña almacenada en el sistema para del administrador.
     */
    @Override
    public String getContrasenna() {
        return contrasenna;
    }

    /**
     *
     * @param contrasenna Guarda la contraseña del administrador.
     */
    @Override
    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    /**
     *
     * @return El nombre almacenado en el sistema  del administrador.
     */
    @Override
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre Guarda el nombre del administrador.
     */
    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return Los apellidos almacenados en el sistema  del administrador.
     */
    @Override
    public String getApellidos() {
        return apellidos;
    }

    /**
     *
     * @param apellidos Guarda los apellidos del administrador.
     */
    @Override
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     *
     * @return el id almacenado en el sistema  del administrador.
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param id Guarda id del administrador.
     */
    @Override
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return el pais almacenado en el sistema del administrador.
     */
    @Override
    public String getPais() {
        return pais;
    }

    /**
     * 
     * @param pais Guarda el país del administrador.
     */
    @Override
    public void setPais(String pais) {
        this.pais = pais;
    }
    
    /**
     * 
     * @param registrado guarda el estado de registro del usuario administrador, como valor booleano
     */
    public void setRegistrado(boolean registrado){
        this.registrado = registrado;
    }
    
    /**
     * 
     * @return el estado de registro del usuario administrador, como valor booleano 
     */
    public boolean getRegistrado(){
        return registrado;
    }
    
    /**
     * Método para devolver en un String los atributos del objeto
     * @return devuelve un String con la información del objeto
     **/
        
    @Override
    public String toString() {
        return "correo=" + correo +
                ", contrasenna=" + contrasenna +
                ", nombre=" + nombre +
                ", apellidos=" + apellidos +
                ", id=" + id +
                ", pais=" + pais;
    }
    
    /**
     * Método para comparar objetos Administrador. Como el sistema definido debe tener solo un usuario administrador,
     * se usará para determinar si hay uno registrado.
     * @param obj instancia de la clase Objeto, para almacenar temporalmente el objeto a comparar
     * @return true si hay un Administrador registrado, false si no
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Administrador other = (Administrador) obj;
        if (this.registrado == false) {
            return false;
        }
        return true;
    }
    
    
}
