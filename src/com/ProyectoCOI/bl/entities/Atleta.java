/*
 * 
 */
package com.ProyectoCOI.bl.entities;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Jorge Zúñiga
 * @version 2
 * 
 * Esta clase gestiona a los usuarios registrados para hacer alguna actividad física en la plataforma. Es una subclase
 * de la clase abstracta Usuario.
 */
public class Atleta extends Usuario{
    private String avatar;
    private String genero;
    private String direccion;
    private LocalDate fechaNac;
    private int edad;
    private double distancia;
    private ArrayList<MetodoDePago> listaMetodosDePago;
    
    /**
     * Constructor sin parámetros
     */
    public Atleta() {
    }
    
    /**
     * Constructor con todos los parámetros, excepto dirección.
     * @param id el número de identificación del atleta
     * @param correo el correo con que registró el atleta
     * @param contrasenna la contraseña creada por el atleta para la aplicación
     * @param nombre el nombre con que registró el atleta
     * @param apellidos los apellidos con que registró el atleta
     * @param pais el país desde el que registra el atleta
     * @param avatar un string con la ruta del avatar del atleta registrado
     * @param genero el género con que se identifica el atleta registrado
     * @param fechaNac la fecha de nacimiento del atleta registrado
     * @param edad la edad del atleta registrado
     * @param distancia la distancia acumulada por un atleta
     * @param listaMetodosDePago la colección de métodos de pago de un atleta
     */
    public Atleta(String id, String correo, String contrasenna, String nombre, String apellidos, String pais, String avatar, 
            String genero, LocalDate fechaNac, int edad, double distancia, ArrayList<MetodoDePago> listaMetodosDePago) {
        super(id, correo, contrasenna, nombre, apellidos, pais);
        this.avatar = avatar;
        this.genero = genero;
        this.fechaNac = fechaNac;
        this.edad = edad;
        this.distancia = distancia;
        this.listaMetodosDePago = listaMetodosDePago;
    }
    
    /**
     * Constructor con todos los parámetros, incluyendo dirección.
     * @param id el número de identificación del atleta
     * @param correo el correo con que registró el atleta
     * @param contrasenna la contraseña creada por el atleta para la aplicación
     * @param nombre el nombre con que registró el atleta
     * @param apellidos los apellidos con que registró el atleta
     * @param pais el país desde el que registra el atleta
     * @param avatar un string con la ruta del avatar del atleta registrado
     * @param genero el género con que se identifica el atleta registrado
     * @param direccion la dirección en Costa Rica desde la que registra el usuario
     * @param fechaNac la fecha de nacimiento del atleta registrado
     * @param edad la edad del atleta registrado
     * @param distancia la distancia acumulada por un atleta
     * @param listaMetodosDePago la colección de métodos de pago de un atleta
     */
    public Atleta(String id, String correo, String contrasenna, String nombre, String apellidos, String pais,String avatar, 
            String genero, String direccion, LocalDate fechaNac, int edad, double distancia, ArrayList<MetodoDePago> listaMetodosDePago) {
        super(id, correo, contrasenna, nombre, apellidos, pais);
        this.avatar = avatar;
        this.genero = genero;
        this.direccion = direccion;
        this.fechaNac = fechaNac;
        this.edad = edad;
        this.distancia = distancia;
        this.listaMetodosDePago = listaMetodosDePago;
    }
    
    /*
    * Constructores SIN lista de métodos de pago
    */
    
    /**
     * Constructor con todos los parámetros, excepto dirección.
     * @param id el número de identificación del atleta
     * @param correo el correo con que registró el atleta
     * @param contrasenna la contraseña creada por el atleta para la aplicación
     * @param nombre el nombre con que registró el atleta
     * @param apellidos los apellidos con que registró el atleta
     * @param pais el país desde el que registra el atleta
     * @param avatar un string con la ruta del avatar del atleta registrado
     * @param genero el género con que se identifica el atleta registrado
     * @param fechaNac la fecha de nacimiento del atleta registrado
     * @param edad la edad del atleta registrado
     * @param distancia la distancia acumulada por un atleta
     */
    public Atleta(String id, String correo, String contrasenna, String nombre, String apellidos, String pais, String avatar, 
            String genero, LocalDate fechaNac, int edad, double distancia) {
        super(id, correo, contrasenna, nombre, apellidos, pais);
        this.avatar = avatar;
        this.genero = genero;
        this.fechaNac = fechaNac;
        this.edad = edad;
        this.distancia = distancia;
    }
    
    /**
     * Constructor con todos los parámetros, incluyendo dirección.
     * @param id el número de identificación del atleta
     * @param correo el correo con que registró el atleta
     * @param contrasenna la contraseña creada por el atleta para la aplicación
     * @param nombre el nombre con que registró el atleta
     * @param apellidos los apellidos con que registró el atleta
     * @param pais el país desde el que registra el atleta
     * @param avatar un string con la ruta del avatar del atleta registrado
     * @param genero el género con que se identifica el atleta registrado
     * @param direccion la dirección en Costa Rica desde la que registra el usuario
     * @param fechaNac la fecha de nacimiento del atleta registrado
     * @param edad la edad del atleta registrado
     * @param distancia la distancia acumulada por un atleta
     */
    public Atleta(String id, String correo, String contrasenna, String nombre, String apellidos, String pais,String avatar, 
            String genero, String direccion, LocalDate fechaNac, int edad, double distancia) {
        super(id, correo, contrasenna, nombre, apellidos, pais);
        this.avatar = avatar;
        this.genero = genero;
        this.direccion = direccion;
        this.fechaNac = fechaNac;
        this.edad = edad;
        this.distancia = distancia;
    }
      
    /**
     * 
     * @return La ruta para acceder y mostrar el avatar del usuario
     */
    public String getAvatar() {
        return avatar;
    }
    
    /**
     * 
     * @param avatar Guarda la ruta para acceder al avatar del usuario
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    
    /**
     * 
     * @return La fecha de nacimiento del usuario, como un atributo LocalDate para poder manipularlo
     */
    public LocalDate getFechaNac() {
        return fechaNac;
    }
    
    /**
     * 
     * @param fechaNac Guarda la fecha de nacimiento ingresada por el usuario
     */
    public void setFechaNac(LocalDate fechaNac) {
        this.fechaNac = fechaNac;
    }
    
    /**
     * 
     * @return El género con que se identifica el usuario
     */
    public String getGenero() {
        return genero;
    }
    
    /**
     * 
     * @param genero Guarda el género con que se identifica el usuario al registrar
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    /**
     * 
     * @return La dirección ingresada por el usuario para recibir medallas, si registró en Costa Rica
     */
    public String getDireccion() {
        return direccion;
    }
    
    /**
     * 
     * @param direccion Guarda la dirección ingresada por el usuario, en Costa Rica, para recibir su medalla
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * 
     * @return La edad del usuario, según la fecha de nacimiento registrada
     */
    public int getEdad() {
        return edad;
    }
    
    /**
     * 
     * @param edad Guarda la edad del usuario, según la fecha de nacimiento que ingresó
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    /**
     * 
     * @return El valor de la distancia acumulada por un usuario en sus actividades físicas
     */
    public double getDistancia() {
        return distancia;
    }
    
    /**
     * 
     * @param distancia Guarda el valor de la distancia que un usuario recorre en sus actividades físicas
     */
    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }
    
    /**
     * 
     * @return El o los métodos de pago registrados por un usuario
     */
    public ArrayList<MetodoDePago> getListaMetodosDePago() {
        return listaMetodosDePago;
    }
    
    /**
     * 
     * @param listaMetodosDePago Guarda los métodos de pago registrados por el usuario
     */
    public void setListaMetodosDePago(ArrayList<MetodoDePago> listaMetodosDePago) {
        this.listaMetodosDePago = listaMetodosDePago;
    }
        
    /**
     * 
     * @return El valor de cada atributo de un objeto "Atleta", convertido a un String
     */
    @Override
    public String toString() {
        return super.toString() + ", avatar=" + avatar + 
                ", fechaNac=" + fechaNac + ", genero=" + genero + ", direccion=" + direccion + ", edad=" + edad +
                "lista de Métodos de Pago=" +listaMetodosDePago;
    }
    
}
