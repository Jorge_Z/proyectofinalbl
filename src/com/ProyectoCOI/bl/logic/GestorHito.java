/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.bl.entities.Hito;
import com.ProyectoCOI.dl.Data;
import java.util.ArrayList;

/**
 * Clase gestora para la clase Hito
 * @author Jorge Zúñiga
 * @version 1
 */
public class GestorHito {
    private Data datos;

    public GestorHito() {
        datos = new Data();
    }
    
    public void registrarHito(String ubicacion, String imagen, String descripcion, String link){
        Hito hito = new Hito(ubicacion, imagen, descripcion, link);
        datos.registrarHito(hito);
    }
    
    public String buscarHito(String ubicacion){
        Hito hitoB = new Hito();
        hitoB.setUbicacion(ubicacion);
        Hito encontrado = datos.buscarHito(hitoB);
        
        if(encontrado != null){
            return encontrado.toString();
        } else {
            return null;
        }
    }
    
    public ArrayList<Hito> listarHitos(){
        return datos.listarHitos();
    }    
}
