/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.bl.entities.Atleta;
import com.ProyectoCOI.bl.entities.Grupo;
import com.ProyectoCOI.dl.Data;
import java.util.ArrayList;

/**
 * Clase gestora para la clase Grupo
 * @author Jorge Zúñiga
 * @version 1
 */
public class GestorGrupo {
    private Data datos;

    public GestorGrupo() {
        datos = new Data();
    }
    
    public void registrarGrupo(String nombre, String imagen){
        Grupo grupo = new Grupo(nombre, imagen);
        datos.registrarGrupo(grupo);
    }
    
    public String buscarGrupo(String nombre){
        Grupo grupoB = new Grupo();
        grupoB.setNombre(nombre);
        Grupo encontrado = datos.buscarGrupo(grupoB);
        
        if(encontrado != null){
            return encontrado.toString();
        } else {
            return null;
        }
    }
    
    public ArrayList<Grupo> listarGrupos(){
        return datos.listarGrupos();
    }
    
    public void agregarAtletaAGrupo(Atleta atleta){
        datos.registrarAtleta(atleta);
    }
}
