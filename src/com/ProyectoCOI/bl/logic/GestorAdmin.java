/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.bl.entities.Administrador;
import com.ProyectoCOI.dl.Data;
import java.util.ArrayList;

/**
 * Clase gestora para la clase Administrador
 * @author Jorge Zúñiga
 * @version 1.2
 */
public class GestorAdmin {
    private Data datos;
    private GestorReto reto;
    private GestorActDeportiva actDeportiva;

    public GestorAdmin() {
        datos = new Data();
    }
    
    public void registrarAdmin(String correo, String contrasenna, String nombre, String apellidos, String cedula, String pais){
        Administrador admin = new Administrador(correo, contrasenna, nombre, apellidos, cedula, pais);
        datos.registrarAdmin(admin);
    }
    
    public String buscarAdmin(String id){
        Administrador adminB = new Administrador();
        adminB.setId(id);
        Administrador encontrado = datos.buscarAdmin(adminB);
        
        if(encontrado != null){
            return encontrado.toString();
        } else {
            return null;
        }
    }
    
     public String mostrarAdmin(){
        return datos.mostrarAdmin().toString();
    }
    
    public void adminRegReto(int codigo, String nombre, String descripcion, String foto, double distancia, 
            String medalla, double costo, String inicio, String fin, ArrayList listaUsuarios, ArrayList listaGrupos, 
            ArrayList listaHitos){
        reto.registrarReto(codigo, nombre, descripcion, foto, distancia, medalla, costo, inicio, fin);
        
    }
    
    public void adminRegActDep(int codigo, String nombre, String icono){
        actDeportiva.registrarActDeportiva(codigo, nombre, icono);
    }
}
