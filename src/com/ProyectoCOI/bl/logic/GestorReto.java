/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.bl.entities.Atleta;
import com.ProyectoCOI.bl.entities.Grupo;
import com.ProyectoCOI.bl.entities.Hito;
import com.ProyectoCOI.bl.entities.Reto;
import com.ProyectoCOI.dl.Data;
import java.util.ArrayList;

/**
 * Clase gestora para la clase Reto
 * @author Jorge Zúñiga
 * @version 1
 */
public class GestorReto {
    private Data datos;

    public GestorReto() {
        datos = new Data();
    }
    
    public void registrarReto(int codigo, String nombre, String descripcion, String foto, double distancia, 
            String medalla, double costo, String inicio, String fin){
        Reto reto = new Reto(codigo, nombre, descripcion, foto, distancia, medalla, costo, inicio, fin);
        datos.registrarReto(reto);
    }
    
    public String buscarReto(int codigo){
        Reto retoB = new Reto();
        retoB.setCodigo(codigo);
        Reto encontrado = datos.buscarReto(retoB);
        
        if(encontrado != null){
            return encontrado.toString();
        } else {
            return null;
        }
    }
    
    public ArrayList<Reto> listarRetos(){
        return datos.listarRetos();
    }
    
    public void agregarAtletaAReto(Atleta atleta){
        datos.registrarAtleta(atleta);
    }
    
    public void agregarGrupoAReto(Grupo grupo){
        datos.registrarGrupo(grupo);
    }
    
    public void agregarHitoAReto(Hito hito){
        datos.registrarHito(hito);
    }
}
