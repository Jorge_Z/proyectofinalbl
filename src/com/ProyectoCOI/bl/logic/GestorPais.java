/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.bl.entities.Pais;
import com.ProyectoCOI.dl.Data;
import java.util.ArrayList;

/**
 * Clase gestora para la clase Pais
 * @author Jorge Zúñiga
 * @version 1
 */
public class GestorPais {
    private Data datos;

    public GestorPais() {
        datos = new Data();
    }
    
    public void registrarPais(int codigo, String nombre){
        Pais pais = new Pais(codigo, nombre);
        datos.registrarPais(pais);
    }
    
    public String buscarPais(String nombre){
        Pais paisB = new Pais();
        paisB.setNombre(nombre);
        Pais encontrado = datos.buscarPais(paisB);
        
        if(encontrado != null){
            return encontrado.toString();
        } else {
            return null;
        }
    }
    
    public ArrayList<Pais> listarPaises(){
        return datos.listarPaises();
    }
}
