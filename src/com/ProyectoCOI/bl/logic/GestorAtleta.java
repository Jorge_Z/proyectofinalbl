/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.bl.entities.Atleta;
import com.ProyectoCOI.bl.entities.MetodoDePago;
import com.ProyectoCOI.dl.Data;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase gestora para la clase Atleta
 * @author Jorge Zúñiga
 * @version 1
 */
public class GestorAtleta {
    
    private Data datos;

    public GestorAtleta() {
        datos = new Data();
    }
    
    public void registrarAtleta(String id, String correo, String contrasenna, String nombre, String apellidos, String pais, 
            String avatar, String genero, LocalDate fechaNac, int edad, double distancia){
        Atleta atleta = new Atleta(id, correo, contrasenna, nombre, apellidos, pais, avatar, genero, fechaNac, edad, distancia);
        datos.registrarAtleta(atleta);
    }
    
    //Separado porque usa dirección, el de arriba no.
    public void registrarAtletaCR(String id, String correo, String contrasenna, String nombre, String apellidos, String pais, 
            String avatar, String genero, String direccion, LocalDate fechaNac, int edad, double distancia){
        Atleta atleta = new Atleta(id, correo, contrasenna, nombre, apellidos, pais, avatar, genero, direccion, fechaNac, edad, distancia);
        datos.registrarAtleta(atleta);
    }
    
    public String buscarAtleta(String id){
        Atleta atletaABuscar = new Atleta();
        atletaABuscar.setId(id);
        Atleta encontrado = datos.buscarAtleta(atletaABuscar);
        
        if(encontrado != null){
            return encontrado.toString();
        } else {
            return null;
        }
    }
    
    public ArrayList<Atleta> listarAtletas(){
        return datos.listarAtletas();
    }
    
    //Idéntico a registrar tarjeta, por ahora. Si la aplicación completa no requiere de un token o ID para el usuario
    //actualmente en el app, puede eliminarse
    public void agregarTarjetaAUsuario(MetodoDePago tarjeta){
        datos.registrarTarjeta(tarjeta);
    }
}
