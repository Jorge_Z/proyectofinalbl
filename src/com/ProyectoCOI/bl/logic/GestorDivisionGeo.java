/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.bl.entities.DivisionGeo;
import com.ProyectoCOI.dl.Data;
import java.util.ArrayList;

/**
 * Clase gestora para la clase DivisionGeo
 * @author Jorge Zúñiga
 * @version 1
 */
public class GestorDivisionGeo {
    private Data datos;

    public GestorDivisionGeo() {
        datos = new Data();
    }
    
    public void registrarDivGeo(int codigo, String nombre, String tipo){
        DivisionGeo divGeo = new DivisionGeo(codigo, nombre, tipo);
        datos.registrarDivGeo(divGeo);
    }
    
    public String buscarDivGeo(int codigo){
        DivisionGeo divGeoB = new DivisionGeo();
        divGeoB.setCodigo(codigo);
        DivisionGeo encontrada = datos.buscarDivGeo(divGeoB);
        
        if(encontrada != null){
            return encontrada.toString();
        } else {
            return null;
        }
    }
    
    public ArrayList<DivisionGeo> listarDivsGeo(){
        return datos.listarDivsGeo();
    }
}
