/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.dl.Data;
import com.ProyectoCOI.bl.entities.ActividadDeportiva;
import java.util.ArrayList;

/**
 * Clase gestora para la clase Actividad Deportiva
 * @author Jorge Zúñiga
 * @version 1
 */
public class GestorActDeportiva {
    
    private Data datos;

    public GestorActDeportiva() {
        datos = new Data();
    }
    
    public void registrarActDeportiva(int codigo, String nombre, String icono){
        ActividadDeportiva actDeportiva = new ActividadDeportiva(codigo, nombre, icono);
        datos.registrarActDeportiva(actDeportiva);
    }
    
    public String buscarActDeportiva(int codigo){
        ActividadDeportiva actDeportB = new ActividadDeportiva();
        actDeportB.setCodigo(codigo);
        ActividadDeportiva encontrada = datos.buscarActDep(actDeportB);
        
        if(encontrada != null){
            return encontrada.toString();
        } else {
            return null;
        }
    }
    
    public ArrayList<ActividadDeportiva> listarActDeportivas(){
        return datos.listarActDeportivas();
    }
}
