/*
 * 
 */
package com.ProyectoCOI.bl.logic;

import com.ProyectoCOI.bl.entities.MetodoDePago;
import com.ProyectoCOI.dl.Data;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase gestora para la clase MetodoDePago
 * @author Jorge Zúñiga
 * @version 1
 */
public class GestorPago {
    private Data datos;

    public GestorPago() {
        datos = new Data();
    }
    
    public void registrarTarjeta(String nombre, int numero, int cvv, String tipo, LocalDate fechaExpiracion){
        MetodoDePago tarjeta = new MetodoDePago(nombre, numero, cvv, tipo, fechaExpiracion);
        datos.registrarTarjeta(tarjeta);
    }
    
    public String buscarTarjeta(String nombre){
        MetodoDePago tarjetaB = new MetodoDePago();
        tarjetaB.setNombre(nombre);
        MetodoDePago encontrado = datos.buscarTarjeta(tarjetaB);
        
        if(encontrado != null){
            return encontrado.toString();
        } else {
            return null;
        }
    }
    
    public ArrayList<MetodoDePago> listarTarjetas(){
        return datos.listarTarjetas();
    }
}
