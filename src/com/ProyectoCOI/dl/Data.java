/*
 * 
 */
package com.ProyectoCOI.dl;

import com.ProyectoCOI.bl.entities.*;

import java.util.ArrayList;

/**
 *  Clase gestora del almacenamiento persistente
 * @author Ahías Steller
 * @version 1
 */
public class Data {

    private ArrayList<ActividadDeportiva> listaActividades;
    private Administrador administrador;
    private ArrayList<Atleta> listaAtletas;
    private ArrayList<DivisionGeo> listaDivisiones;
    private ArrayList<Grupo> listaGrupos;
    private ArrayList<Hito> listaHitos;
    private ArrayList<MetodoDePago> listaPagos;
    private ArrayList<Pais> listaPaises;
    private ArrayList<Reto> listaRetos;

    public Data() {
        listaActividades = new ArrayList<>();
        listaAtletas = new ArrayList<>();
        listaDivisiones = new ArrayList<>();
        listaGrupos = new ArrayList<>();
        listaHitos = new ArrayList<>();
        listaPagos = new ArrayList<>();
        listaPaises = new ArrayList<>();
        listaRetos = new ArrayList<>();
    }

    public void registrarActDeportiva(ActividadDeportiva actividad){
        listaActividades.add(actividad);
    }

    public ArrayList<ActividadDeportiva> listarActDeportivas(){
        return (ArrayList<ActividadDeportiva>)  listaActividades.clone();
    }

    public ActividadDeportiva buscarActDep(ActividadDeportiva actividadABuscar) {
        for (ActividadDeportiva actividad: listaActividades){
            if (actividad.equals(actividadABuscar)){
                return actividad;
            }
        }
        return null;
    }

    public void registrarAdmin(Administrador admin){
        administrador = admin;
    }

    public Administrador mostrarAdmin(){
        return administrador;
    }

    public Administrador buscarAdmin(Administrador adminABuscar){
        if (administrador.equals(adminABuscar)){
            return adminABuscar;
        }
        return null;
    }

    public void registrarAtleta(Atleta atleta) {
        listaAtletas.add(atleta);
    }

    public ArrayList<Atleta> listarAtletas(){
        return (ArrayList) listaAtletas.clone();
    }

    public Atleta buscarAtleta(Atleta atletaABuscar){
        for (Atleta atleta:listaAtletas){
            if (atleta.equals(atletaABuscar)){
                return atleta;
            }
        }
        return null;
    }

    public void registrarDivGeo(DivisionGeo divisionGeo) {
        listaDivisiones.add(divisionGeo);
    }

    public ArrayList<DivisionGeo> listarDivsGeo(){
        return (ArrayList) listaDivisiones.clone();
    }

    public DivisionGeo buscarDivGeo(DivisionGeo divisionABuscar){
        for (DivisionGeo division:listaDivisiones){
            if (division.equals(divisionABuscar)){
                return division;
            }
        }
        return null;
    }

    public void registrarGrupo(Grupo grupo){
        listaGrupos.add(grupo);
    }

    public ArrayList<Grupo> listarGrupos(){
        return (ArrayList) listaGrupos.clone();
    }

    public Grupo buscarGrupo(Grupo grupoABuscar){
        for(Grupo grupo: listaGrupos){
            if (grupo.equals(grupoABuscar)){
                return grupo;
            }
        }
        return null;
    }

    public void registrarHito(Hito hito){
        listaHitos.add(hito);
    }

    public ArrayList<Hito> listarHitos(){
        return (ArrayList) listaHitos.clone();
    }

    public Hito buscarHito(Hito hitoABuscar){
        for (Hito hito: listaHitos){
            if (hito.equals(hitoABuscar)){
                return hito;
            }
        }
        return null;
    }

    public void registrarTarjeta(MetodoDePago metodoDePago){
        listaPagos.add(metodoDePago);
    }

    public ArrayList<MetodoDePago> listarTarjetas(){
        return (ArrayList) listaPagos.clone();
    }

    public MetodoDePago buscarTarjeta(MetodoDePago tarjetaABuscar){
        for (MetodoDePago metodoDePago:listaPagos){
            if (metodoDePago.equals(tarjetaABuscar)){
                return metodoDePago;
            }
        }
        return null;
    }

    public void registrarPais(Pais pais){
        listaPaises.add(pais);
    }

    public ArrayList<Pais> listarPaises(){
        return (ArrayList) listaPaises.clone();
    }

    public Pais buscarPais(Pais paisABuscar){
        for (Pais pais:listaPaises){
            if (pais.equals(paisABuscar)){
                return pais;
            }
        }
        return null;
    }

    public void registrarReto(Reto reto){
        listaRetos.add(reto);
    }

    public ArrayList<Reto> listarRetos(){
        return (ArrayList) listaRetos.clone();
    }

    public Reto buscarReto(Reto retoABuscar){
        for (Reto reto:listaRetos){
            if (reto.equals(retoABuscar)){
                return reto;
            }
        }
        return null;
    }





}
