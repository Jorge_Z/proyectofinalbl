/*
 * 
 */
package com.ProyectoCOI.tl;

import com.ProyectoCOI.bl.logic.*;
import com.ProyectoCOI.ui.UI;

import java.io.IOException;
import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.YEARS;

/**
 *
 * @author jz600
 */
public class Controlador {

    private UI interfaz;
    private GestorActDeportiva gestorActDeportiva;
    private GestorAdmin gestorAdmin;
    private GestorAtleta gestorAtleta;
    private GestorDivisionGeo gestorDivisionGeo;
    private GestorGrupo gestorGrupo;
    private GestorHito gestorHito;
    private GestorPago gestorPago;
    private GestorPais gestorPais;
    private GestorReto gestorReto;


    public Controlador(){
        interfaz = new UI();
        gestorActDeportiva = new GestorActDeportiva();
        gestorAdmin = new GestorAdmin();
        gestorAtleta = new GestorAtleta();
        gestorDivisionGeo = new GestorDivisionGeo();
        gestorGrupo = new GestorGrupo();
        gestorHito = new GestorHito();
        gestorPago = new GestorPago();
        gestorPais = new GestorPais();
        gestorReto = new GestorReto();
    }

    public void start()throws IOException {
        int opcion = -1;
        do {
            interfaz.mostrarMenu();
            opcion = interfaz.leerOpcion();
            procesarOpcion(opcion);
        }while(opcion != 0);

    }

    public void procesarOpcion(int opcion) throws IOException {
        switch (opcion)
        {
            case 1:
                registrarUsuario();
                break;
            case 2:
                mostrarUsuarios();
                break;
            case 3:
                registrarAdministrador();
                break;
            case 4:
                mostrarAdministrador();
                break;
            case 5:
                registrarActidadDeport();
                break;
            case 6:
                mostrarActidadDeport();
                break;
            case 7:
                registrarDivisionGeo();
                break;
            case 8:
                mostrarDivisionGeo();
                break;
            case 9:
                registrarGrupo();
                break;
            case 10:
                mostrarGrupo();
                break;
            case 11:
                registrarReto();
                break;
            case 12:
                mostrarRetos();
                break;
            case 13:
                registrarHito();
                break;
            case 14:
                mostrarHito();
                break;
            case 0:
                interfaz.impMens("Gracias, vuelva pronto");
                System.exit(0);
                break;
            default:
                interfaz.impMens("No existe esa opción");
        }
    }

    public void registrarUsuario()throws IOException {

        interfaz.impMens("---Registro de usuarios---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese el correo:");
        String correo = interfaz.leerTexto();
        interfaz.impMens("Ingrese la contraseña:");
        String contrasenna = interfaz.leerTexto();
        interfaz.impMens("Ingrese el nombre:");
        String nombre = interfaz.leerTexto();
        interfaz.impMens("Ingrese sus apellidos:");
        String apellidos = interfaz.leerTexto();
        System.out.println("Ingrese su cédula:");
        String cedula = interfaz.leerTexto();
        interfaz.impMens("Ingrese su país:");
        String pais = interfaz.leerTexto();
        interfaz.impMens("Ingrese su avatar:");
        String avatar = interfaz.leerTexto();
        interfaz.impMens("Ingrese su fecha de nacimiento(yyyy-mm-dd):");
        String fechaNac = interfaz.leerTexto();
        LocalDate fechaNacimiento = LocalDate.parse(fechaNac);
        interfaz.impMens("Ingrese su género:");
        String genero = interfaz.leerTexto();
        interfaz.impMens("Ingrese su dirección:");
        String direccion = interfaz.leerTexto();
        //interfaz.impMens("Ingrese su edad:");
        //int edad = Integer.parseInt(interfaz.leerTexto());
        interfaz.impMens("Ingrese la distancia recorrida el día de hoy:");
        double distancia = Double.parseDouble(interfaz.leerTexto());

        int edad = (int)YEARS.between(fechaNacimiento, LocalDate.now());
        gestorAtleta.registrarAtleta(cedula, correo, contrasenna, nombre, apellidos, pais, avatar, genero, fechaNacimiento, edad, distancia);
        
        interfaz.impMens("El usuario fue registrado correctamente");

    }

    public void mostrarUsuarios() {
        gestorAtleta.listarAtletas().forEach(atleta -> interfaz.impMens(atleta.toString()));
    }

    public void registrarAdministrador()throws IOException {
        interfaz.impMens("---Registro de administrador---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese el correo:");
        String correo = interfaz.leerTexto();
        //administrador.setCorreo(in.readLine());
        interfaz.impMens("Ingrese la contraseña:");
        String contrasenna = interfaz.leerTexto();
        //administrador.setContrasenna(in.readLine());
        interfaz.impMens("Ingrese el nombre:");
        String nombre = interfaz.leerTexto();
        //administrador.setNombre(in.readLine());
        interfaz.impMens("Ingrese sus apellidos:");
        String apellidos = interfaz.leerTexto();
        //administrador.setApellidos(in.readLine());
        interfaz.impMens("Ingrese su cédula:");
        String cedula = interfaz.leerTexto();
        //administrador.setId(in.readLine());
        interfaz.impMens("Ingrese su país:");
        //administrador.setPais(in.readLine());
        String pais = interfaz.leerTexto();
        gestorAdmin.registrarAdmin(correo, contrasenna, nombre, apellidos, cedula, pais);

        interfaz.impMens("El administrador fue registrado correctamente");


    }

    public void mostrarAdministrador() {
        interfaz.impMens(gestorAdmin.mostrarAdmin());
    }

    public void registrarActidadDeport() throws IOException {
        interfaz.impMens("---Registro de actividad deportiva---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese el codigo de la actividad:");
        int codigo = Integer.parseInt(interfaz.leerTexto());
        interfaz.impMens("Ingrese el nombre de la actividad:");
        String nombre = interfaz.leerTexto();
        interfaz.impMens("Ingrese el ícono de la actividad:");
        String icono = interfaz.leerTexto();

        gestorActDeportiva.registrarActDeportiva(codigo, nombre, icono);

        interfaz.impMens("Actividad deportiva registrada correctamente");
    }

    public void mostrarActidadDeport() {
        gestorActDeportiva.listarActDeportivas().forEach(actividadDeportiva -> interfaz.impMens(actividadDeportiva.toString()));
    }

    public void registrarDivisionGeo()throws IOException {
        interfaz.impMens("---Registro de división geográfica---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese el codigo de la división:");
        int codigo = Integer.parseInt(interfaz.leerTexto());
        interfaz.impMens("Ingrese el nombre de la división:");
        String nombre = interfaz.leerTexto();
        interfaz.impMens("Ingrese el tipo de la división:");
        String tipo = interfaz.leerTexto();

        gestorDivisionGeo.registrarDivGeo(codigo, nombre, tipo);
        interfaz.impMens("División geográfica registrada correctamente");
    }

    public void mostrarDivisionGeo() {
        gestorDivisionGeo.listarDivsGeo().forEach(divisionGeo -> interfaz.impMens(divisionGeo.toString()));
    }

    public void registrarGrupo() throws IOException {
        interfaz.impMens("---Registro de grupo---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese el nombre del grupo:");
        String nombre = interfaz.leerTexto();
        interfaz.impMens("Ingrese la imagen del grupo:");
        String imagen = interfaz.leerTexto();
        gestorGrupo.registrarGrupo(nombre, imagen);

        interfaz.impMens("Grupo registrado correctamente");
    }

    public void mostrarGrupo() {
        gestorGrupo.listarGrupos().forEach(grupo -> interfaz.impMens(grupo.toString()));
    }

    public void registrarReto() throws IOException {
        interfaz.impMens("---Registro de retos---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese el código del reto: ");
        int codigo = Integer.parseInt(interfaz.leerTexto());
        interfaz.impMens("Ingrese el nombre del reto: ");
        String nombre = interfaz.leerTexto();
        interfaz.impMens("Ingrese la descripción del reto: ");
        String descripcion = interfaz.leerTexto();
        interfaz.impMens("Ingrese la foto del reto: ");
        String foto = interfaz.leerTexto();
        interfaz.impMens("Ingrese la distancia del reto:");
        double distancia = Double.parseDouble(interfaz.leerTexto());
        interfaz.impMens("Ingrese la medalla del reto: ");
        String medalla = interfaz.leerTexto();
        interfaz.impMens("Ingrese el costo del reto");
        double costo = Double.parseDouble(interfaz.leerTexto());
        interfaz.impMens("Ingrese el inicio del reto:");
        String inicio = interfaz.leerTexto();
        interfaz.impMens("Ingrese el fin del reto: ");
        String fin = interfaz.leerTexto();
        gestorReto.registrarReto(codigo, nombre, descripcion, foto, distancia, medalla, costo, inicio, fin);
        
        interfaz.impMens("El reto fue registrado correctamente");

    }

    public void mostrarRetos(){
        gestorReto.listarRetos().forEach(reto -> interfaz.impMens(reto.toString()));
    }

    public void registrarHito()throws IOException {
        interfaz.impMens("---Registro de hitos---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese la ubicación:");
        String ubicacion = interfaz.leerTexto();
        interfaz.impMens("Ingrese la imagen:");
        String imagen = interfaz.leerTexto();
        interfaz.impMens("Ingrese la descripción:");
        String descripcion = interfaz.leerTexto();
        interfaz.impMens("Ingrese el link:");
        String link = interfaz.leerTexto();
        gestorHito.registrarHito(ubicacion, imagen, descripcion, link);
        
        interfaz.impMens("Hito registrado correctamente");
    }

    public void mostrarHito() {
        gestorHito.listarHitos().forEach(hito -> interfaz.impMens(hito.toString()));
    }

    public void registrarPago()throws IOException {
        interfaz.impMens("---Registro de métodos de Pago---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese el nombre tal cual aparece en la tarjeta:");
        String nombre = interfaz.leerTexto();
        interfaz.impMens("Ingrese número de la tarjeta:");
        int numero = Integer.parseInt(interfaz.leerTexto());
        interfaz.impMens("Ingre el CVV de la tarjeta:");
        int cvv = Integer.parseInt(interfaz.leerTexto());
        interfaz.impMens("Ingrese el tipo de la tarjeta");
        String tipo = interfaz.leerTexto();
        interfaz.impMens("Ingrese la fecha de vencimiento de la tarjeta(yyyy-mm-dd):");
        String fechaVenc = interfaz.leerTexto();
        LocalDate fecha = LocalDate.parse(fechaVenc);
        gestorPago.registrarTarjeta(nombre, numero, cvv, tipo, fecha);

        interfaz.impMens("Pago registrado correctamente");
    }

    public void mostrarPagos(){
        gestorPago.listarTarjetas().forEach(metodoDePago -> interfaz.impMens(metodoDePago.toString()));
    }

    public void registrarPais()throws IOException{
        interfaz.impMens("---Registro de países---");
        interfaz.impMens("Por favor, digite la información que se solicita a continuación");
        interfaz.impMens("Ingrese el código del país: ");
        int codigo = Integer.parseInt(interfaz.leerTexto());
        interfaz.impMens("Ingrese el nombre del país: ");
        String pais = interfaz.leerTexto();
        gestorPais.registrarPais(codigo, pais);

        interfaz.impMens("País registrado correctamente");
    }

    public void mostrarPaises(){
        gestorPais.listarPaises().forEach(pais -> interfaz.impMens(pais.toString()));
    }

}
